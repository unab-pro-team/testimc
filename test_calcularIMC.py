import unittest
from imcFunctions import calcularIMC

class TestCalcularIMC(unittest.TestCase):
    def test_valid_IMC(self):
        self.assertEqual(calcularIMC(80.0,1.72),27.04,msg='Error en IMC')
        self.assertEqual(calcularIMC(10.0,0.8),15.62,msg='Error en IMC')
        self.assertEqual(calcularIMC(320.0,1.75),104.49,msg='Error en IMC')
        self.assertEqual(calcularIMC(0.35,0.55),1.16,msg='Error en IMC')
        
    def test_invalid_IMC(self):
        self.assertRaises(ZeroDivisionError, calcularIMC, 80.0, 0)
