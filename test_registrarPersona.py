import unittest
from imcFunctions import registrarPersona



class TestRegistrarPersona(unittest.TestCase):

    personaEjemplo = {
            'nombre':'Alex Perez',
            'Rut': '12058145-3',
            'email': 'AlexP@email.com',
            'password': 'AlexP1205'
        }

    listaPersonas = [personaEjemplo]

    def test_register(self):
        ret = registrarPersona('Alicia Gonzalez', '17753221-5', 'AliciaG@email.com', 'AliciaG1790', self.listaPersonas)
        self.assertEqual(len(ret), 2)
        self.assertIn(self.personaEjemplo, ret)
        self.assertIn(
            {
                'nombre': 'Alicia Gonzalez',
                'Rut': '17753221-5',
                'email': 'AliciaG@email.com',
                'password': 'AliciaG1790'
            },
            ret
        )