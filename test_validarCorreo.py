import unittest
from imcFunctions import validarCorreo

class TestValidarCorreo(unittest.TestCase):
    def test_valid_email(self):
        self.assertTrue(validarCorreo("correo@valido.cl"))
        self.assertTrue(validarCorreo("correo123@valido.cl"))
        self.assertTrue(validarCorreo("correo@valido123.cl"))
        self.assertTrue(validarCorreo("correo.con.puntos@valido.cl"))
        self.assertTrue(validarCorreo("!#$%&'*+/=?^_`{|}~-@valido.cl"))
    
    def test_invalid_email(self):
        self.assertFalse(validarCorreo(""))
        self.assertFalse(validarCorreo("sin_arroba"))
        self.assertFalse(validarCorreo("dos.arrobas@@mail.cl"))
        self.assertFalse(validarCorreo("@sin_usuario.cl"))
        self.assertFalse(validarCorreo("sin_organizacion@.cl"))
        self.assertFalse(validarCorreo("sin_tipo@mail"))