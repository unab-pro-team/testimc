import unittest
from imcFunctions import calcularContraseña

class TestCalcularContraseña(unittest.TestCase):
    personas = [
        {
            "nombre": "Manuel",
            "rut": "17753221-K",
            "email": "manuel.carvajal@unab.cl"
        },
        {
            "nombre": "Eduardo",
            "rut": "2179401-5",
            "email": "eduardo.pavez@unab.cl"
        },
        {
            "nombre": "Gaston",
            "rut": "15993182-K",
            "email": "gaston.marsano@unab.cl"
        },
    ]

    def test_password(self):
        self.assertEqual(
            calcularContraseña(self.personas[0]),
            'manuel.carvajal1775'
        )
        self.assertTrue(
            calcularContraseña(self.personas[1]),
            "eduardo.pavez2179"
        )
        self.assertTrue(
            calcularContraseña(self.personas[1]),
            "gaston.marsano1599"
        )