import unittest
from imcFunctions import mostrarEstadoNutricional

class TestMostrarEstadoNutricional(unittest.TestCase):
    def test_valid_Estado(self):

        self.assertEqual(mostrarEstadoNutricional(0.0,'F'),'BAJO PESO',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(20.0,'F'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(20.1,'F'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(23.8,'F'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(23.9,'F'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(24.0,'F'),'OBESIDAD LEVE',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(28.9,'F'),'OBESIDAD LEVE',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(29.0,'F'),'OBESIDAD SEVERA',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(37.0,'F'),'OBESIDAD SEVERA',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(37.1,'F'),'OBESIDAD MUY SEVERA',msg='Error en Estado Nutricional')

        self.assertEqual(mostrarEstadoNutricional(0,'M'),'BAJO PESO',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(20,'M'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(20.1,'M'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(24.8,'M'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(24.9,'M'),'NORMAL',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(25.0,'M'),'OBESIDAD LEVE',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(29.9,'M'),'OBESIDAD LEVE',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(30.0,'M'),'OBESIDAD SEVERA',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(40.0,'M'),'OBESIDAD SEVERA',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(40.1,'M'),'OBESIDAD MUY SEVERA',msg='Error en Estado Nutricional')


    def test_invalid_IMC(self):
        self.assertEqual(mostrarEstadoNutricional(-10,'M'),'BAJO PESO',msg='Error en Estado Nutricional')
        self.assertEqual(mostrarEstadoNutricional(-10,'F'),'BAJO PESO',msg='Error en Estado Nutricional')