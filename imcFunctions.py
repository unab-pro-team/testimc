import re

def registrarPersona(nombre, rut, email, password, listaPersonas):
    '''
    función que permite guardar en una lista los datos de nombre, run, correo electrónico, contraseña.
    '''
    datospersonas = {"nombre":nombre, "Rut": rut, "email":email, "password" : password}
    # Incorporado gracias a escenario de prueba. Se agrega la nueva persona a la lista original
    listaPersonas.append(datospersonas)
    return listaPersonas


def calcularIMC(peso, altura):
    '''
    función que permite calcular el IMC con el peso y altura
    '''
    if (altura == 0):
        # Incorporado gracias a escenario de prueba
        raise ZeroDivisionError("Error: División por cero.")
    imc = peso/(altura**2)
    print(f"IMC = {round(imc,2)}")
    return round(imc,2)

def mostrarEstadoNutricional(imc, sexo):
    '''
    función que permite mostrar el estado nutricional a partir de un IMC dado
    '''
    if imc < 20: estado = 'BAJO PESO'
    elif sexo == 'M':
        # Incorporado gracias a escenario de prueba. Se agrega >=
        if imc <= 24.9: estado = 'NORMAL'
        elif imc <= 29.9: estado = 'OBESIDAD LEVE'
        elif imc <= 40: estado = 'OBESIDAD SEVERA'
        else: estado = 'OBESIDAD MUY SEVERA'
    else:
        if imc <= 23.9: estado = 'NORMAL'
        elif imc <= 28.9: estado = 'OBESIDAD LEVE'
        elif imc <= 37: estado = 'OBESIDAD SEVERA'
        else: estado = 'OBESIDAD MUY SEVERA'
    print(estado)
    return estado


def validarCorreo(email):
    '''
    función que permite validar el correo ingresado, ya que existe @
    '''
    email = email.strip()
    # Incorporado gracias a escenario de prueba. Se agregan símbolos válidos
    expresion_regular = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    email_valido = re.match(expresion_regular,email) is not None
    if not email_valido:
        print('El email no tiene un formato valido.')
        return False
    print('El email es válido')
    return True

def calcularContraseña(persona):
    '''
    función que permite calcular la contraseña de la persona, la contraseña será la concatenación del usuario del correo electrónico concatenado con los cuatro primeros números del rut
    '''
    # Incorporado gracias a escenario de prueba. Se corrige el índice de dígitos a tomar en el rut
    password = persona["email"].split("@")[0] + persona["rut"][:4]
    print(f"La password es {password}")
    return password

if __name__ == "__main__":
    listaPersonas = []
    print("Bienvenido.")
    while (True):
        print("Elija una opción: ")
        print("1 Registrar Persona")
        print("2 Calcular IMC")
        print("3 Mostrar Estado Nutricional")
        print("4 Validar Correo")
        print("5 Calcular Contraseña")
        print("6 Salir")

        option = input(">> ")
        if (option == '1'):
            nombre = input('Nombre: ')
            rut = input('RUT: ')
            email = input('Email: ')
            password = input('Password: ')
            print(registrarPersona(nombre, rut, email, password, listaPersonas))
            input('Presione enter para continuar...')
            continue

        if (option == '2'):
            peso = float(input('Peso: '))
            altura = float(input('Altura: '))
            calcularIMC(peso, altura)
            input('Presione enter para continuar...')
            continue

        if (option == '3'):
            imc = float(input('IMC: '))
            sexo = input('Sexo: ')
            mostrarEstadoNutricional(imc, sexo)
            input('Presione enter para continuar...')
            continue

        if (option == '4'):
            email = input('Email: ')
            validarCorreo(email)
            input('Presione enter para continuar...')
            continue

        if (option == '5'):
            rut = input('RUT: ')
            email = input('Email: ')
            persona = {'rut': rut, 'email': email}
            calcularContraseña(persona)
            input('Presione enter para continuar...')
            continue

        if (option not in ('1', '2', '3', '4', '5')): break